open_project -reset "proj"
set_top process_packet
set cflags "-std=c++11"
add_files firmware/tcp.cc -cflags ${cflags}
add_files -tb ref/tcp_ref.cc -cflags ${cflags}
add_files -tb tcp_test.cpp -cflags ${cflags}
set npkt 6
for {set i 1} {$i <= $npkt} {incr i} {
    add_files -tb ref/data/arp_tcp_hello/in.$i
    #add_files -tb ref/data/test/in.$i 
}

open_solution -reset "solution"
set_part {xcku15p-ffva1760-2-e}
create_clock -period 3.1 

csim_design -argv $npkt
#if { [info exists env(DO_SYNTH)] && $env(DO_SYNTH) == "1"  } {
    csynth_design
    #cosim_design  -argv $npkt -trace_level all 
#}
exit