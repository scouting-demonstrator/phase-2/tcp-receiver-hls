#ifndef tcp_hls_ref_h
#define tcp_hls_ref_h

#include <cstdio>
#include <cstdint>
#include <algorithm>
#include <cassert>
#include <vector>

class processor {
    public:
        processor(uint64_t mac, uint32_t ip, uint16_t port, bool dupack, FILE *fout) : 
            myip_(ip), mymac_(mac), listeners_(1, Listener(port)), dupack_(dupack),
            fout_(fout) {
        }
        processor(uint64_t mac, uint32_t ip, bool dupack, FILE *fout) : 
            myip_(ip), mymac_(mac), listeners_(), dupack_(dupack),
            fout_(fout) {
        }
        void addListener(uint16_t port) { listeners_.emplace_back(port); }
        bool process(const uint8_t *pck_in, unsigned int len_in, uint8_t * pkt_out, unsigned int & len_out, uint64_t & tcp_data_out_size) ;
        bool process_arp(const uint8_t *pck_in, unsigned int len_in, uint8_t * pkt_out, unsigned int & len_out) ;
        bool process_ipv4(const uint8_t *pck_in, unsigned int len_in, uint8_t * pkt_out, unsigned int & len_out, uint64_t & tcp_data_out_siz) ;
        bool process_tcp(const uint8_t *pck_in, unsigned int len_in, uint32_t srcip, uint32_t dstip, unsigned int len, bool badpckt, uint8_t * pkt_out, unsigned int & len_out, uint64_t & tcp_data_out_siz) ;
        void closeFile();
    private:
        uint32_t myip_;
        uint64_t mymac_;
        bool dupack_;
        FILE *fout_;
        enum class State { Listen = 0, SynReceived = 1, Established = 2, FinSent = 3 };
        struct Listener {
            uint16_t myport_;
            State state_;
            uint32_t expectedSeqNo_;
            uint64_t receivedDataSize_;
            Listener(uint16_t port) : myport_(port), state_(State::Listen), expectedSeqNo_(0) {}
        };
        std::vector<Listener> listeners_;

        static constexpr uint16_t ETH_TYPE_ARP = 0x0806;
        static constexpr uint16_t ETH_TYPE_IPV4 = 0x0800;
        static constexpr uint64_t ETH_BROADCAST_MAC = (1l<<48)-1;
        static constexpr unsigned int ETH_HDR_LEN = 6+6+2;
        static constexpr unsigned int ARP_LEN = 28;
        static constexpr uint8_t IP_PROTO_TCP = 0x06;
        static constexpr unsigned int IPV4_HDR_MIN_LEN = 20;
        static constexpr unsigned int TCP_HDR_SYN_LEN = 20+8;
        static constexpr unsigned int TCP_HDR_DATA_LEN = 20;
        static constexpr uint32_t TCP_INITIAL_SYN = 37;
        static constexpr uint32_t TCP_MY_MSS = 0x2300; // 0x5b4;

        uint32_t sum16(const uint8_t *pkt, unsigned int len) ;
        uint16_t finishChecksum(uint32_t sum) ;
};

#endif